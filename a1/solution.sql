
-- A.

SELECT * FROM songs WHERE song_name LIKE "%d%";


-- B.

SELECT * FROM songs WHERE length < 230;

-- C.

SELECT albums.album_title, songs.song_name, songs.length FROM 
	albums JOIN songs ON albums.id = songs.album_id;

-- D.

SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id WHERE album_title LIKE "%d%";

-- E.

SELECT * FROM albums WHERE id IN (1, 3, 17,14) ORDER BY album_title DESC; 

-- F.

SELECT * FROM albums JOIN songs ON albums.id = songs.album_id
ORDER BY album_title DESC; 


